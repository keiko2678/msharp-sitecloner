﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pri.LongPath;
using System.Reflection;
using System.Diagnostics;

#pragma warning disable GCop102 
#pragma warning disable GCop412
namespace MsharpSiteCloner
{
    public class Program
    {
        public static Settings Settings = new Settings();

        static void Main()
        {
            ShowCredits();

            var path = new DirectoryInfo(@"C:\Deploys\Brookson Development");
            if (path.Exists)
                new SiteCloner(path, 6);
        }

        private static void ShowCredits()
        {
            var assembly = Assembly.GetExecutingAssembly();
            var fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
            string version = fvi.FileVersion;


            Console.Write("\n  **************************************" +
                          "\n  *                                    *" +
                          "\n  *  M# SiteCloner                     *" +
                          "\n  *                                    *" +
                                  "\n  *  Version: " + version + new string(' ', 25 - version.Length) + "*" +
                          "\n  *  By: P.C. Kofstad                  *" +
                          "\n  *                                    *" +
                          "\n  **************************************\n\n");
        }
    }
}
