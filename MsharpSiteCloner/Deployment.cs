﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using MSharp.Framework;
using Pri.LongPath;

namespace MsharpSiteCloner
{
    public class Deployment
    {
        public BaseProject BaseProject;
        public int InstanceNumber;

        public Deployment(BaseProject baseProject, int instanceNumber)
        {
            BaseProject = baseProject;
            InstanceNumber = instanceNumber;
        }

        public string DatabasName
        {
            get
            {
                return DeploymentName + ".Temp";
            }
        }

        public DirectoryInfo DeployFolder
        {
            get
            {
                return new DirectoryInfo(Path.Combine(Program.Settings.DeployFolder, DeploymentName));
            }
        }

        public string DeploymentName
        {
            get
            {
                return BaseProject.Name + "." + InstanceNumber;
            }
        }
    }

    public class BaseProject
    {
        public string Name
        {
            get
            {
                return DatabasName.Remove(DatabasName.LastIndexOf('.'));
            }
        }

        private string _DatabaseName;
        public string DatabasName
        {
            get
            {
                if (_DatabaseName.IsEmpty())
                    _DatabaseName = GetDatabaseName();
                return _DatabaseName;
            }
        }

        public DirectoryInfo ProjectFolder;
        public List<Deployment> Deployments;

        public BaseProject(DirectoryInfo projectFolder, int numberOfinstances = 0)
        {
            ProjectFolder = projectFolder;
            Deployments = new List<Deployment>();

            for (int i = 1; i <= numberOfinstances; i++)
            {
                Deployments.Add(new Deployment(this, i));
            }
        }

        public string GetDatabaseConnectionString()
        {
            var configFile = Path.Combine(ProjectFolder.FullName, "Website", "Web.config").AsFile();
            if (configFile.Exists())
            {
                var document = XDocument.Load(configFile.FullName).Root;
                return document.Element("connectionStrings")?.Element("add")?.Attribute("connectionString").Value;
            }

            return null;
        }

        public string GetDatabaseName()
        {
            return GetDatabaseConnectionString().TrimBefore("Database=").TrimStart("Database=").TrimAfter(";");
        }

    }
}
