﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Web.Administration;
using Pri.LongPath;
using System.Net;

namespace MsharpSiteCloner
{
#pragma warning disable GCop412
    internal class IisOperations
    {
        private const string SERVEERMANAGERFILE = "C:\\Windows\\System32\\inetsrv\\config\\applicationHost.config";

        private Deployment _Deployment;

        internal IisOperations(Deployment deploy)
        {
            _Deployment = deploy;
        }

        internal bool RegisterWebsite()
        {
            if (SiteAllreadyRegistered())
                RemoveWebsite();

            try
            {
                using (var iisManager = new ServerManager(SERVEERMANAGERFILE))
                {
                    var appPool = iisManager.ApplicationPools.Add(_Deployment.DeploymentName);
                    appPool.ProcessModel.IdentityType = ProcessModelIdentityType.LocalSystem;
                    appPool.AutoStart = true;

                    iisManager.Sites.Add(_Deployment.DeploymentName, "http", "*:80:" + GetBinding(), FindLocalPathMount());
                    iisManager.Sites[_Deployment.DeploymentName].ApplicationDefaults.ApplicationPoolName = _Deployment.DeploymentName;
                    iisManager.Sites[_Deployment.DeploymentName].ServerAutoStart = true;

                    iisManager.CommitChanges();
                }
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        private string FindLocalPathMount()
        {
            var mountFolder = Path.Combine(_Deployment.DeployFolder.FullName, "Website");
            if (mountFolder.AsDirectory().Exists())
                return mountFolder;

            var list = Directory.GetFiles(_Deployment.DeployFolder.FullName, "Index.html", System.IO.SearchOption.AllDirectories);
            if (list.Any())
                return list.ToList().OrderBy(x => x.Length).First()?.AsFile()?.Directory.ToString();

            return _Deployment.DeployFolder.FullName;
        }

        private string GetBinding() => (_Deployment.DeploymentName + "." + Program.Settings.UrlPrefix + "." + Dns.GetHostName() + Program.Settings.Domain).ToLower();

        public string GetURL()
        {
            return @"http://" + GetBinding() + "/";
        }

        internal bool RemoveWebsite()
        {
            if (FindSite() == null)
            {
                RemoveAppPoolIfExists();
                return true;
            }

            try
            {
                using (var iisManager = new ServerManager(SERVEERMANAGERFILE))
                {
                    var appPool = iisManager.ApplicationPools.FirstOrDefault(x => x.Name == _Deployment.DeploymentName);
                    iisManager.Sites.Remove(iisManager.Sites[_Deployment.DeploymentName]);
                    if (appPool != null)
                        iisManager.ApplicationPools.Remove(appPool);
                    iisManager.CommitChanges();
                }
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        internal bool SiteAllreadyRegistered() => FindSite() != null;

        private Site FindSite()
        {
            using (var iisManager = new ServerManager(SERVEERMANAGERFILE))
            {
                var sites = iisManager.Sites.Where(x => x.Name == _Deployment.DeploymentName);
                return sites.FirstOrDefault() ?? null;
            }
        }

        private void RemoveAppPoolIfExists()
        {
            using (var iisManager = new ServerManager(SERVEERMANAGERFILE))
            {
                var appPool = iisManager.ApplicationPools.FirstOrDefault(x => x.Name == _Deployment.DeploymentName);
                if (appPool != null)
                    iisManager.ApplicationPools.Remove(appPool);
                iisManager.CommitChanges();
            }
        }

    }
}
