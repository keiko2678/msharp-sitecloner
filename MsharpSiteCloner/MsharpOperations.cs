﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pri.LongPath;
using System.Xml.Linq;

namespace MsharpSiteCloner
{
    class MsharpOperations
    {
        public static void UpdateDatabaseString(Deployment deployment)
        {
            var configFile = Path.Combine(deployment.DeployFolder.FullName, "Website", "Web.config");

            if (!configFile.AsFile().Exists())
                throw new Exception("No configfile found, can't update connectionstring");

            var document = XDocument.Load(configFile);
            document.Root.Element("connectionStrings")?.Element("add")?.Attribute("connectionString")
                .SetValue(deployment.BaseProject.GetDatabaseConnectionString()
                    .Replace(deployment.BaseProject.DatabasName,deployment.DatabasName));
            document.Save(configFile);
        }
    }
}
