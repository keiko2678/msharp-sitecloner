﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using MSharp.Framework;

#pragma warning disable GCop412
namespace MsharpSiteCloner
{
    public class Settings
    {
        private const string DEFAULTWORKFOLDER = "C:\\Deploys";
        private const string DEFAULTURLPREFIX = "test";
        private const string DEFAULTDOMAIN = ".g.e";

        private string _DeployFolder;
        public string DeployFolder
        {
            get
            {
                if (_DeployFolder.IsEmpty())
                    _DeployFolder = DEFAULTWORKFOLDER;
                return _DeployFolder;
            }
            set
            {
                _DeployFolder = value;
            }
        }

        private string _UrlPrefix;
        public string UrlPrefix
        {
            get
            {
                if (_UrlPrefix.IsEmpty())
                    _UrlPrefix = DEFAULTURLPREFIX;
                return _UrlPrefix;
            }
            set
            {
                _UrlPrefix = value;
            }
        }

        private string _Domain;
        public string Domain
        {
            get
            {
                if (_Domain.IsEmpty())
                    _Domain = DEFAULTDOMAIN;
                return _Domain;
            }
            set
            {
                _Domain = value;
            }
        }

    }
}
