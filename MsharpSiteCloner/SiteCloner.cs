﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pri.LongPath;
using System.Diagnostics;

namespace MsharpSiteCloner
{
    class SiteCloner
    {
        private BaseProject BaseProject;
        private List<Deployment> Deployments
        {
            get { return BaseProject?.Deployments ?? new List<Deployment>(); }
        }

        public SiteCloner(DirectoryInfo directory, int numberOfInstances = 1)
        {

            BaseProject = new BaseProject(directory, numberOfInstances);

            foreach (var deploy in Deployments)
            {
                var iis = new IisOperations(deploy);

                if (!iis.RemoveWebsite())
                {
                    Console.WriteLine("Can't detach " + deploy.DeploymentName + " from IIS - aborting");
                }
                else
                {
                    if (deploy.DeployFolder.Exists)
                    {
                        Console.WriteLine("Existing folder found for " + deploy.DeploymentName + " - remvoing it");
                        FileOperations.DeleteDirectory(deploy.DeployFolder.FullName);
                    }

                    Console.WriteLine("Cloning to " + deploy.DeployFolder.FullName);
                    FileOperations.CopyDirectory(BaseProject.ProjectFolder, deploy.DeployFolder);

                    MsharpOperations.UpdateDatabaseString(deploy);

                    if (iis.RegisterWebsite())
                    {
                        Console.WriteLine("Registered on: " + iis.GetURL() + "\n");
                    }
                }

            }
        }
    }
}
